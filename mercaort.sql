-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 14-11-2019 a las 18:57:55
-- Versión del servidor: 5.7.17-log
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mercaort`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `Id` int(11) NOT NULL,
  `titulo` varchar(15) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `Id_User` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`Id`, `titulo`, `descripcion`, `Id_User`) VALUES
(1, '[MATEMATICA]', '[ESTE ES LA CATEGORIA DE LA MATERIA MATEMATICA]', 0),
(2, 'LENGUA', '', 0),
(3, 'HISTORIA', '', 0),
(4, 'GEOGRAFIA', '', 0),
(5, 'INGLES', '', 0),
(6, 'ARTE', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificaciones`
--

CREATE TABLE `notificaciones` (
  `id` int(11) NOT NULL,
  `cuerpo` varchar(50) NOT NULL,
  `Id_user` int(11) NOT NULL,
  `Id_comprador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id_producto` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `cuerpo` varchar(255) DEFAULT NULL,
  `fecha` varchar(15) DEFAULT NULL,
  `Id_Categoria` int(11) DEFAULT NULL,
  `Id_User` int(11) DEFAULT NULL,
  `Id_Estado` int(11) DEFAULT NULL,
  `precio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id_producto`, `titulo`, `img`, `cuerpo`, `fecha`, `Id_Categoria`, `Id_User`, `Id_Estado`, `precio`) VALUES
(32, 'fvvyv cdjhg f', 'images/185px-Marx_old.jpg', ' gg jy bvfv', NULL, 2, 16, NULL, NULL),
(33, 'la biblia', 'images/descarga.jfif', 'es para coger', NULL, 3, 15, NULL, NULL),
(34, 'boca', 'images/benny grebsnare.jpg', 'hola', NULL, 3, 16, NULL, NULL),
(35, 'libro de historia 4to a', 'images/hola.jfif', 'contiene 6 unidades ', NULL, 2, 15, NULL, NULL),
(36, 'libro', 'images/1295378_1.jpg', 'lsdghdjjasiudnasuihas', NULL, 1, 16, NULL, NULL),
(37, 'libro matmatica', 'images/ara', 'esta re copado shshijbdsuiiwekjhuiewbfjkusdaduikjfhuiwjkhuiyukjsdihsihkjds', NULL, 3, 16, NULL, NULL),
(38, 'libro de lenguia', 'images/peruga.jpg', 'dksjkksksjs', NULL, 2, 16, NULL, NULL),
(39, 'boca', 'images/Brockhaus_and_Efron_Jewish_Encyclopedia_e15_334-0.jpg', 'smslkljsl@issks', NULL, 3, 16, NULL, 300);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `Id` int(11) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `password` varchar(15) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`Id`, `Email`, `password`, `usuario`, `apellido`) VALUES
(11, '', 'root ', 'pepe', 'tuvieja'),
(13, '', 'jaja', 'jaja', 'mja'),
(14, '', 'hola', 'ivan', 'das'),
(15, '', '1234', 'hola', 'pepe'),
(16, '', 'pepe', 'kaka', 'retrete');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `notificaciones`
--
ALTER TABLE `notificaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `Id_Categoria` (`Id_Categoria`),
  ADD KEY `Id_User` (`Id_User`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `notificaciones`
--
ALTER TABLE `notificaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`Id_Categoria`) REFERENCES `categoria` (`Id`),
  ADD CONSTRAINT `producto_ibfk_2` FOREIGN KEY (`Id_User`) REFERENCES `usuario` (`Id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
