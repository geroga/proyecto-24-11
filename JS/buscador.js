
$(document).ready(function(){
 
     
 $("#buscar").click(function(){
     var consulta;
         //hacemos focus al campo de búsqueda
        $("#busqueda").focus();
 
        //comprobamos si se pulsa una tecla
 
              //obtenemos el texto introducido en el campo de búsqueda
              consulta = {
                consulta: $(".busqueda").val(),
              };
 
              //hace la búsqueda
 
              $.ajax({
                    type: "GET",
                    url: "php/buscador.php",
                    data: consulta,
                    dataType: "json",
                

                    success: function(data)
                    {
                      $(".conte2").empty();
                      if (data.status=='ok') 
                      {
                        var out = '';
                       $.each(data,function(i,v){
                          out+= "<div class=''card mt-3' style='width: 18rem;'>";
                          out+= "<img src='images/"+data[i].img+"' class='card-img-top'>";
                          out+="<div class='card-body'>";
                          out+="<h5 class='d-flex card-title'>"+data[i].titulo+"</h5>";
                          out+="<a href='#' class='d-flex card-text'>"+data[i].cuerpo+"</a>";
                          out+="<br><br>";
                          out+="<p class='card-text price'>"+data[i].precio+"</p>";
                          out+="<hr><button href='http://mpago.la/2V3gjp' class='btn btn-primary'>COMPRAR</button>";
                          out+="<a href='#' class='card-link'>Ver mas</a>";
                          out+="</div></div>";
                          out+="<div class='divider'></div>";
                          });
                        $(".conte2").append(out);
                      }
                      else
                      {
                        $(".conte2").append(data[0]);
                      }
                    },
              });
            });
        });
