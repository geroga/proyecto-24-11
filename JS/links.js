

let productos = []
let btn_agregarproducto = document.getElementById('aceptar')
let btn_verproductos = document.getElementById('ver')

btn_agregarproducto.addEventListener('click', agregarProducto)
btn_verproductos.addEventListener('click', mostrarProductos)


class Producto{
    constructor(descripcion, precio, imagen, categoria){
        this.descripcion = descripcion
        this.precio = precio
        this.imagen = imagen
        this.categoria = categoria
    }
}

function agregarProducto(){
    let descripcion = document.getElementById('descripcion').value
    let precio = document.getElementById('precio').value
    let imagen = document.getElementById('imagen').value
    let categoria = document.getElementById('Categoria').value

    let producto = new Producto(descripcion, precio, imagen, categoria)
    productos.push(producto)

    let campos = ['descripcion', 'precio', 'imagen', 'Categoria']
    limpiarCampos(campos)
}

function mostrarProductos(){
    productos.forEach(function(producto){
        let div_card = document.createElement('div')
        div_card.classList.add('card', 'mt-3')
        div_card.style.width = "18rem"

        let img = document.createElement('img')
        img.classList.add('card-img-top')
        img.src = producto.imagen
        let div_cardbody = document.createElement('div')
        div_cardbody.classList.add('card-body')

        let cardtitle = document.createElement('h5')
        cardtitle.classList.add('d-flex', 'card-title')
        let link = document.createElement('a')
        link.classList.add('d-flex', 'card-text')
        link.href = "#"
        let br = document.createElement('br')
        let text = document.createElement('p')
        text.classList.add('card-text', 'price')
        let hr = document.createElement('hr')
        let button = document.createElement('button')
        button.classList.add('btn', 'btn-primary')

        let text_title = document.createTextNode(producto.descripcion)
        let link_text = document.createTextNode(producto.categoria)
        let price_text = document.createTextNode(producto.precio)
        let button_text = document.createTextNode('COMPRAR')


        cardtitle.appendChild(text_title)
        link.appendChild(link_text)
        text.appendChild(price_text)
        button.appendChild(button_text)
    

        div_card.appendChild(img)

        div_cardbody.appendChild(cardtitle)
        div_cardbody.appendChild(link)
        div_cardbody.appendChild(br)
        div_cardbody.appendChild(br)
        div_cardbody.appendChild(text)
        div_cardbody.appendChild(hr)
        div_cardbody.appendChild(button)

        div_card.appendChild(div_cardbody)

        let container = document.getElementById('cards')
        container.appendChild(div_card)
    })
    let body = document.getElementById('body')
    body.classList.remove('modal-open')
    
    document.getElementById('exampleModalScrollables').classList.remove('show')

    body.lastChild.remove()
}

function limpiarCampos(campos){
    campos.forEach(function(id){
        document.getElementById(id).value = ""
    });
}