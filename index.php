<?php
session_start();
include 'php/conexion.php';
?>
<!DOCTYPE html>
<html lang="es" style="background-color: #000a12;">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LINK</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" type="image/jpg" href="IMG/icono.png">
    <link rel="stylesheet" href="css/styles.css" type="text/css">
        <link rel="stylesheet" type="text/javascript"href="JS/materialize.css" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/adda1ce5ac.js"></script>
</head>


<body id='body'>
  <div class="modal fade" id="exampleModalScrollables" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalScrollableTitle">Datos del producto</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body flex-column justify-content-between">
              <div id="cc">
                  <input id="imagen" type="text" placeholder="Imagen del producto" class="FotoProducto">
              </div>
              <div id="cc">
                <input id="precio" type="text" placeholder="Precio del Producto" class="PrecioProducto" style="width: 70%">
              </div>
              <div id="cc">
                <input id="descripcion" type="text" placeholder="Nombre del producto" class="DescripsionProducto" style="width: 90%">
              </div>
              <div id="cc">
                <input id="Categoria" type="text" placeholder="Categoria del Producto" class="CategoriaProducto" style="width: 90%">
              </div>
          </div>
            <div class="modal-footer flex-row-reverse">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="ver" type="button" class="btn btn-info">Ver cambios</button>
                <button id="aceptar" type="button" class="btn btn-primary">Add</button>
            </div>
        </div>
      </div>
    </div>
    
      <!--HEADER-->

  <header class="header d-flex justify-content-between">
    <div class="logo">
      <a href="LINK.html"><img src="IMG/logo.png" class="img-fluid" alt="Responsive image" style="width: 80px;"></a>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="container h-100">
        <div class="d-flex justify-content-center h-100">
          <div class="searchbar">
            <input class="search_input" type="text" name="" placeholder="Nombre, Autor O ISBN...">
            <a href="buscadorFront.php" class="search_icon"><i class="fas fa-search"></i></a>
          </div>
        </div>
      </div>
    </nav>
  </header>

    <!--NAVBAR-->
    <nav class="d-flex navbar navbar-expand-lg navbar-light justify-content-center" id="colornyf">
      <div class="d-flex justify-content-center">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link"style="color: white;" href="#">LIBROS</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" style="color: white;" href="#" >NOVEDADES</a>
            </li>
            <li class="nav-item" id="ingresar">
              <a class="nav-link"style="color: white;" href="login.html">INGRESAR</a>
            </li>
          
          
              <li class="nav-item dropdown" id="sesion"><?php if($_SESSION['Id']!=''): ?>
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;"></a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown"></a>
                <a class="dropdown-item"  style="color: #1C2833;"href="publicaciones.php">MIS PUBLICACIONES</a>
                <a class="dropdown-item" style="color: #1C2833;" href="">salir<?=@session_destroy();?></a>
                <a class="dropdown-item"style="color: #1C2833;" href="registrar-publicacion.html">vender</a>
                     </div>
            <?php endif;?>
          </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="contact.html" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;">
                CONTACTO
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="https://www.instagram.com/" style="color: #1C2833;" target="_blank">INSTAGRAM</a>
                <a class="dropdown-item" href="https://www.facebook.com/" style="color: #1C2833;" target="_blank">FACEBOOK</a>
                <div class="dropdown-divider" ></div>
                <a class="dropdown-item" href="#" style="color: #1C2833">OTROS</a>
              </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;"><i class="large material-icons">notifications_active</i></a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item"style="color: #1C2833;">INGRESA PARA VER TUS NOTIFICACIONES!</a>
                </div>
              </li>
          </div>
        </div>
      </nav>

    <!--CARRUSEL-->

    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
              <img src="https://tap-multimedia-1113.nyc3.digitaloceanspaces.com/slider/2006/large/1200x300_SPINETTA.jpg" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="https://tap-multimedia-1113.nyc3.digitaloceanspaces.com/slider/1958/large/exilart.jpg" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="https://tap-multimedia-1113.nyc3.digitaloceanspaces.com/slider/2000/large/Banner_web_cuspide_1200x300.jpg" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="https://tap-multimedia-1113.nyc3.digitaloceanspaces.com/slider/1951/large/banner_cuspide_1200x300.jpg" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="https://tap-multimedia-1113.nyc3.digitaloceanspaces.com/slider/2003/large/OSCURA_SOLITARIA.jpg" class="d-block w-100" alt="...">
          </div>
        </div>
      </div>


    <!--CARDS-->

      <div class="d-flex flex-column flex-start cards">
        <h3 class="pl-4 pt-3">
          Los
           <a href="" style="color: red;">mas vendidos de la semana</a>
        </h3>
        <div class="d-flex justify-content-around flex-wrap" id="cards">
          <div class="card mt-3" style="width: 18rem;">
            <img src="https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/9788498386653.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="d-flex card-title">LIBRO EJ</h5>
              <a href="#" class="d-flex card-text">Ciencia ficcion</a>
              <br>
              <br>
              <p class="card-text price">$5.000</p>
              <hr>
              <button class="btn btn-primary">COMPRAR</button>
              <a href="#" class="card-link">Ver mas</a>
            </div>
          </div>
          <div class="card mt-3" style="width: 18rem;">
            <img src="https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/9788498386653.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="d-flex card-title">LIBRO EJ</h5>
              <a href="#" class="d-flex card-text">Ciencia ficcion</a>
              <br>
              <br>
              <p class="card-text price">$8.000</p>
              <hr>
              <button href="#" class="btn btn-primary">COMPRAR</button>
              <a href="#" class="card-link">Ver mas</a>
            </div>
          </div>
          <div class="card mt-3" style="width: 18rem;">
            <img src="https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/9788498386653.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="d-flex card-title">LIBRO EJ</h5>
              <a href="#" class="d-flex card-text">Ciencia ficcion</a>
              <br>
              <br>
              <p class="card-text price">$22.800</p>
              <hr>
              <button class="btn btn-primary">COMPRAR</button>
              <a href="#" class="card-link">Ver mas</a>
            </div>
          </div>
          <div class="card mt-3" style="width: 18rem;">
            <img src="https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/9788498386653.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="d-flex card-title">LIBRO EJ</h5>
              <a href="#" class="d-flex card-text">Ciencia ficcion</a>
              <br>
              <br>
              <p class="card-text price">$48.500</p>
              <hr>
              <button class="btn btn-primary">COMPRAR</button>
              <a href="#" class="card-link">Ver mas</a>
            </div>
          </div>
          <div class="card mt-3" style="width: 18rem;">
            <img src="https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/9788498386653.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="d-flex card-title">LIBRO EJ</h5>
              <a href="#" class="d-flex card-text">Ciencia ficcion</a>
              <br>
              <br>
              <p class="card-text price">$70.300</p>
              <hr>
              <button class="btn btn-primary">COMPRAR</button>
              <a href="#" class="card-link">Ver mas</a>
            </div>
          </div>
          <div class="card mt-3" style="width: 18rem;">
            <img src="https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/9788498386653.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="d-flex card-title">LIBRO EJ</h5>
              <a href="#" class="d-flex card-text">Ciencia ficcion</a>
              <br>
              <br>
              <p class="card-text price">$3.399</p>
              <hr>
              <button class="btn btn-primary">COMPRAR</button>
              <a href="#" class="card-link">Ver mas</a>
            </div>
          </div>
          <div class="card mt-3" style="width: 18rem;">
            <img src="https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/9788498386653.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="d-flex card-title">LIBRO EJ</h5>
              <a href="#" class="d-flex card-text">Ciencia ficcion</a>
              <br>
              <br>
              <p class="card-text price">$28.000</p>
              <hr>
              <button class="btn btn-primary">COMPRAR</button>
              <a href="#" class="card-link">Ver mas</a>
            </div>
          </div>
          <div class="card mt-3" style="width: 18rem;">
            <img src="https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/9788498386653.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="d-flex card-title">LIBRO EJ</h5>
              <a href="#" class="d-flex card-text">Ciencia ficcion</a>
              <br>
              <br>
              <p class="card-text price">$9.000</p>
              <hr>
              <button class="btn btn-primary">COMPRAR</button>
              <a href="#" class="card-link">Ver mas</a>
            </div>
          </div>
        </div>
      </div>
      
<!-- Footer -->
<footer id="footer" class="page-footer font-small stylish-color-dark pt-4">

    <!-- Footer Links -->
    <div id="links" class="container text-center text-md-left">
  
      <!-- Grid row -->
      <div class="row">
  
        <!-- Grid column -->
        <div class="col-md-4 mx-auto">
  
          <!-- Content -->
          <h5 class="font-weight-bold text-uppercase mt-3 mb-4" style="color:white">LINK</h5>
          <p style="color:white">Somos una empresa dedicada en su totalidad a la venta de libros.
            Buscamos prestar el mejor servicio de Argentina. Gracias!</p>
  
        </div>
        <!-- Grid column -->
  
        <hr class="clearfix w-100 d-md-none">
  
        <!-- Grid column -->
        <div class="col-md-2 mx-auto">
  
          <!-- Links -->
          <h5 class="font-weight-bold text-uppercase mt-3 mb-4" style="color: white;">Mi usuario</h5>
  
          <ul class="list-unstyled">
            <li>
              <a href="#!">Estado de mi compra</a>
            </li>
            <li>
              <a href="#!">Registrarse</a>
            </li>
            <li>
              <a href="#!">Suscripcion</a>
            </li>
          </ul>
  
        </div>
        <!-- Grid column -->
        <!-- Grid column -->
  
        <hr class="clearfix w-100 d-md-none">
  
        <!-- Grid column -->
        <div class="col-md-2 mx-auto">
  
          <!-- Links -->
          <h5 class="font-weight-bold text-uppercase mt-3 mb-4" style="color:white">Guía</h5>
  
          <ul class="list-unstyled">
            <li>
              <a href="#!">Promociones</a>
            </li>
            <li>
              <a href="#!">Devoluciones</a>
            </li>
            <li>
              <a href="#!">Preguntas</a>
            </li>
          </ul>
  
        </div>
        <!-- Grid column -->
  
      </div>
      <!-- Grid row -->
  
    </div>
    <!-- Footer Links -->
  
    <hr>

  
    <hr>
  
    <!-- Social buttons -->
    <ul class="list-unstyled list-inline text-center">
      <li class="list-inline-item">
        <a href="https://www.facebook.com/" target="_blank" class="btn-floating btn-fb mx-1">
          <i class="fab fa-facebook-f"> </i>
        </a>
      </li>
      <li class="list-inline-item">
        <a href="https://www.twitter.com/" target="_blank" class="btn-floating btn-tw mx-1">
          <i class="fab fa-twitter"> </i>
        </a>
      </li>
      <li class="list-inline-item">
        <a href="https://www.gmail.com/" target="_blank"  class="btn-floating btn-gplus mx-1">
          <i class="fab fa-google-plus-g"> </i>
        </a>
      </li>
      <li class="list-inline-item">
        <a href="https://www.linkedin.com/" target="_blank" class="btn-floating btn-li mx-1">
          <i class="fab fa-linkedin-in"> </i>
        </a>
      </li>
    </ul>
    <!-- Social buttons -->
  
    <!-- Copyright -->
    <div id="copyr" class="footer-copyright text-center py-3" style="color:grey">© 2018 Copyright:
      <a href="https://mdbootstrap.com/education/bootstrap/"> link.com</a>
    </div>
    <!-- Copyright -->

  </footer>

      <script type="text/javascript" src="JS/links.js"></script>
      <script type="text/javascript" src="JS/link.js"></script>
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script type="text/javascript" src="JS/jquery-3.3.1.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
